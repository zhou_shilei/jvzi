import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '@/views/login' // /index.vue是可以省略的

import Layout from '@/views/layout'
import Home from '@/views/home' // 首页
import Video from '@/views/video' // 视频
import Question from '@/views/question' // 问答
import User from '@/views/user' // 用户

Vue.use(VueRouter)

const routes = [
  // 用户登陆
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  // 主页
  {
    path: '/',
    name: 'layout',
    component: Layout,
    children: [
      { path: '', component: Home },
      { path: '/video', component: Video },
      { path: '/question', component: Question },
      { path: '/user', component: User }
    ]
  }
]
const router = new VueRouter({
  routes
})

export default router
