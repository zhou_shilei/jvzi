import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 脚手架自动创建的vuex.store
import store from './store'
// 引入vant组件库
// 全局导入，全部的vant可以直接使用
// import Vant from 'vant'
// import { Lazyload } from 'vant'
import Vant, { Lazyload } from 'vant' // 懒加载
// 导入事件总线
import eventBus from './utils/eventBus'

import 'vant/lib/index.css'

// 全局样式
import '@/styles/index.less'
// 它会根据的手机尺寸来调整rem的基准值：html标签上的font-size
import 'amfe-flexible'
import { relativeTime } from '@/utils/date-time.js'

Vue.use(Vant)
Vue.use(Lazyload)

Vue.config.productionTip = false

// 注册全局过滤器，对时间进行处理
Vue.filter('relativeTime', relativeTime)
// Vue.filter('relativeTime', function (val) {
//   //  原来是： '2018-09-29'
//   //  转成： 2年前

//   // 解决方案：
//   // 1. 自己写代码
//   // 2. day.js库

//   return val.substr(0, 10)
//   // return '相对' + val
// })

// 把事件总线挂在原型对象上
// 则在组件内部，可以直接通过this.$eventBus来访问这个事件总线对象
Vue.prototype.$eventBus = eventBus

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
